{
  $schema: https://vega.github.io/schema/vega-lite/v2.json
  data: {
    # URL object is a context-aware query to Elasticsearch
    url: {
      # The %-enclosed keys are handled by Kibana to modify the query
      # before it gets sent to Elasticsearch. Context is the search
      # filter as shown above the dashboard. Timefield uses the value 
      # of the time picker from the upper right corner.
      %context%: true
      %timefield%: date
      index: pomber.github.io
      body: {
        size: 10000
        _source: ["date", "deaths", "country"]
      }
    }
    # We only need the content of hits.hits array
    format: {property: "hits.hits"}
  }
  # Parse timestamp into a javascript date value
  transform: [
    {calculate: "toDate(datum._source['date'])", as: "time"}
  ]
  # Draw a circle, with x being the time field, and y - number of bytes
  mark: circle
  encoding: {
    x: {field: "time", type: "temporal", axis: {title: null}}
    y: {field: "_source.deaths", type: "quantitative", axis: {title: null}}
    size: {field: "_source.deaths", type: "quantitative", legend: null}
      color: {field:"_source.country", type:"nominal", legend: {title:"asdf"}}
      shape: {field:"_source.country", type:"nominal"}
  }
}