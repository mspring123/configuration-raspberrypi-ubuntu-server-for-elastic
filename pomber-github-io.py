#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
Created on 24.06.2021
 
@author: michaelspring123@web.de
'''

"""
Transforms the data from CSSEGISandData/COVID-19 into a json file. Available at https://pomber.github.io/covid19/timeseries.json. Updated three times a day using GitHub Actions.

The json contains the number of Coronavirus confirmed cases, deaths, and recovered cases for every country and every day since 2020-1-22

# source
https://github.com/pomber/covid19

# structure

{
  "Afghanistan": [
    {
      "date": "2020-1-22",
      "confirmed": 0,
      "deaths": 0,
      "recovered": 0
    },
    {
      "date": "2020-1-23",
      "confirmed": 0,
      "deaths": 0,
      "recovered": 0
    }
"""

# to handle  data retrieval
import urllib3
import requests
# from urllib3 import request
# to handle certificate verification
import certifi
# to manage json data
import json
# for pandas dataframes
import pandas as pd

# uncomment below if installation needed (not necessary in Colab)
# !pip install certifi
download_link = 'https://pomber.github.io/covid19/timeseries.json'

myUsername = ""  # Placeholder, no username is required
myPassword = ""  # Placeholder, no password is required

response = requests.get(download_link, auth=(myUsername, myPassword), headers={'User-Agent': 'Mozilla'})
if response.status_code is not 200:
      # This means something went wrong.
      raise ApiError('GET /tasks/ {}'.format(response.status_code))
else:
      print(response.headers['content-type'])
      if response.status_code is 200:
            content = json.loads(response.content.decode('utf-8'))  # the whole nested content

# The data are not presented here as simple objects; the data are categorized into countries and their content in time series
from pandas.io.json import json_normalize
# features = json_normalize(content['features'])
# features = pd.DataFrame(json_normalize(content['features']))

stu_df = pd.DataFrame(json_normalize(content))  # Inversion of the values, country per column
col_names = ['confirmed', 'date', 'country', 'deaths', 'recovered']
features = pd.DataFrame(columns=col_names)  # Bare structure for the normalized content

# Theoretically, one would not have to normalize the data for ElasticSearch, since object-oriented databases can use partially structured data. For types converting to pandas, however, this is easier. In addition, the existing Elastic Client does not have to be rewritten.

# Normalization of the data
for (columnName, columnData) in stu_df.iteritems(): 
#       print('Colunm Name : ', columnName)
      structured = pd.DataFrame(json_normalize(content[columnName]))
      # Using DataFrame.insert() to add a column 
      structured.insert(2, "country", columnName, True)
      structured['date'] = pd.to_datetime(structured['date'])  # Convert to a date type
      list = ['confirmed', 'deaths', 'recovered']  # Columns that have to be converted to integers
      for type_c in list:
            structured[type_c] = structured[type_c].astype(int)
#       print(structured)
      features = features.append(structured)  # Append the frames
print(features.head())

"""
#      confirmed       date   country  deaths  recovered
# 0            0 2020-01-22  Zimbabwe       0          0
# 1            0 2020-01-23  Zimbabwe       0          0
"""

from datetime import datetime


def safe_date(date_value):
      return (
            pd.to_datetime(date_value) if not pd.isna(date_value)
                  else  datetime(1970, 1, 1, 0, 0)
      )


features['date'] = features['date'].apply(safe_date)
# features.to_csv('/Users/asdf/Desktop/asdf.csv', index = False, encoding='utf-8-sig')

import requests
import json

url = 'http://localhost:9200/asdf?pretty'
body = {
  "settings": {
    "number_of_shards": 3,
    "number_of_replicas": 2
  }
}

from elasticsearch import Elasticsearch
from elasticsearch import helpers
import hashlib

es = Elasticsearch("http://localhost:9200")

# index the documents
count = 0
actions = []
for (index, row) in features.iterrows():
    generated_key = str(row['date']).replace(' ', '_') + '-' + row['country']  # generates the artificial key
    action = {
            "_index": "pomber.github.io",
            "_type": "_doc",
            "_id": hashlib.sha256(str(generated_key).encode('utf-8')).hexdigest(),
            "_source": {
                "country": str(row['country']),
                "date": datetime.isoformat(row['date']),
                "confirmed": int(row['confirmed']),
                "recovered": int(row['recovered']),
                "deaths": int(row['deaths']),
                "properties": {
                    "inserted": datetime.now(),
                    "source": "https://github.com/pomber/covid19"
                    }
                }
        }
    actions.append(action)
    count += 1
    if count % 200 == 0:
        try:
            helpers.bulk(es, actions)
            to_elastic_string = ""
            actions = []
        except:
            print('damn it, we have a problem')
# successfully inserted into Elasticsearch
print('successfully inserted into elasticsearch.')

"""
# after the transformation, a document looks like this
# {'_index': 'pomber.github.io', '_type': '_doc', '_id': '5df934fb4b34ba4219ffe7405e527df53f230dd6793d6dc4b47f8c719e8991e5', '_source': {'country': 'Zimbabwe', 'date': '2021-03-21T00:00:00', 'confirmed': 36665, 'recovered': 34269, 'deaths': 1512, 'properties': {'inserted': datetime.datetime(2021, 8, 28, 22, 2, 48, 775961), 'source': 'https://github.com/pomber/covid19'}}}
"""

res = es.search(index="pomber.github.io", body={"query": {"match_all": {}}})
print("Got %d Hits:" % res['hits']['total']['value'])
for hit in res['hits']['hits']:
    print("%(country)s %(date)s: %(confirmed)s" % hit["_source"])

