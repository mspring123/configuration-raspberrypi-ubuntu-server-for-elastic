#!/usr/bin/env python
# -*- coding: utf-8 -*-

from elasticsearch import Elasticsearch

import pandas as pd
from elasticsearch import Elasticsearch
import elasticsearch.helpers

es = Elasticsearch('localhost',
        port=9200)

# GET pomber/_search
# {
  # "size": 0,
  # "aggs": {
    # "uniqe_country": {
      # "terms": {
        # "field": "country.keyword",
        # "size": 200
      # }
    # }
  # }
# }

body = {
  "size": 0,
  "aggs": {
    "uniqe_country": {
      "terms": {
        "field": "country.keyword",
        "size": 200
      }
    }
  }
}

results = elasticsearch.helpers.scan(es, query=body, index="pomber")
df = pd.DataFrame.from_dict([document['_source'] for document in results])
print(df.head())
