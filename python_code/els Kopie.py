#!/usr/bin/env python
# -*- coding: utf-8 -*-

from elasticsearch import Elasticsearch

es = Elasticsearch('localhost:9200')
res = es.search(index="pomber", body={"query": {"match": {"country": "Afghanistan"}}})
# print("%d documents found" % res['hits']['total'])
# for doc in res['hits']['hits']:
#     print("%s) %s" % (doc['_id'], doc['_source']['content']))
print(res['hits']['total'])


import pandas as pd
from elasticsearch import Elasticsearch
import elasticsearch.helpers


es = Elasticsearch('localhost',
        port=9200)

body={"query": {"match": {"country": "Germany"}}}
# body={"query": {"match_all": {}}}

body={"query": {"match": {"country": "Germany"}}}
results = elasticsearch.helpers.scan(es, query=body, index="pomber")
df = pd.DataFrame.from_dict([document['_source'] for document in results])
print(df.head())

from app import app
from app import server
import dash_html_components as html
import dash_core_components as dcc


app.layout = html.Div([html.H1('coronavirus dashboard',
                               style={
                                      'textAlign': 'center',
                                      "background": "yellow"}),
    html.Div(id='dd-output-container'),
                       dcc.Graph(
                           id='graph-1',
                           figure={
                               'data': [
                                   {'x': df['date'], 'y': df['confirmed'], 'type': 'line', 
                                   'name': 'confirmed', 'line':dict(color='red')},
                                   {'x': df['date'], 'y': df['deaths'], 'type': 'line', 'name': 'deaths',
                                   'line':dict(color='black')},
                                   {'x': df['date'], 'y': df['recovered'], 'type': 'line', 'name': 'recovered',
                                   'line':dict(color='blue')
                                   }
                               ],
                               'layout': {
                                   'title': 'increasing number of cases',
                                     }
                                 }
                            ),
                            ], style={
                                "background": "#000080"}
                         )


if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0')


