#!/usr/bin/env python
# -*- coding: utf-8 -*-

import dash
import dash_html_components as html
import dash_core_components as dcc
import plotly.express as px
import numpy as np


@app.callback(
    Output("histogramm", "figure"),
    [Input("verteilung-dropdown", "value"), Input("n-input", "value")],
)
def hist(verteilung, n):
    data = None
    if verteilung == "normal":
        data = np.random.normal(loc=0, scale=1, size=n)
    elif verteilung == "binomial":
        data = np.random.binomial(n=10, p=0.5, size=n)
    elif verteilung == "chisquared":
        data = np.random.chisquare(df=5, size=n)
    return px.histogram(data)

 
app = dash.Dash(__name__)
 
app.layout = html.Div(
    children=[
        html.H1(children="Verteilungen"),
        html.Div(
            children=[
                html.H2("Inputs"),
                html.Div(
                    children=[
                        html.P("Verteilung"),
                        dcc.Dropdown(
                            id="verteilung-dropdown",
                            options=[
                                {"label": "Normal", "value": "normal"},
                                {"label": "Binomial", "value": "binomial"},
                                {"label": "Chi²", "value": "chisquared"},
                            ],
                            value="normal",
                        ),
                    ],
                ),
                html.Div(
                    children=[
                        html.P("Stichprobengröße"),
                        dcc.Input(
                            id="n-input",
                            placeholder="Stichprobengröße",
                            type="number",
                            value=100,
                        ),
                    ],
                ),
            ],
            # mit style kann man CSS-Formatierungen verwenden
            style={
                "backgroundColor": "#DDDDDD",
                "maxWidth": "800px",
                "padding": "10px 20px",
            },
        ),
        html.Div(
            children=[
                html.H2("Histogramm"),
                dcc.Graph(id="histogramm"),
            ],
            style={
                "backgroundColor": "#DDDDDD",
                "maxWidth": "800px",
                "marginTop": "10px",
                "padding": "10px 20px",
            },
        ),
    ]
)
 
if __name__ == "__main__":
    app.run_server(debug=True)


