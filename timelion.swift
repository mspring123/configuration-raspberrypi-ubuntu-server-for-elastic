.es(index=pomber.github.io,
    timefield='date',
    metric='max:confirmed')
  .label('max confirmed')                    
  .title('timelion over time')
  .color('#03a9f4'), 
.es(index=pomber,
    timefield='date',
    metric='max:confirmed')
  .if(gt,
      1000000,
      .es(index=pomber.github.io,
          timefield='date',
          metric='max:confirmed'),
      null)
    .label('warning')
    .color('#FFCC11')                 
    .lines(width=5),                  
.es(index=pomber.github.io,
    timefield='date',
    metric='max:confirmed')
  .if(gt,
      10000000,
      .es(index=pomber.github.io,
          timefield='date',
          metric='max:confirmed'),
      null)
    .label('severe')
    .color('red')
    .lines(width=5),
.es(index=pomber.github.io,
    timefield='date',
    metric='max:confirmed')
  .mvavg(10)
  .label('mvavg')
  .lines(width=2)
  .color(#5E5E5E)
  .legend(columns=4, position=nw) 