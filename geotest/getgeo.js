/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

const axios = require('axios');
const csv = require('jquery-csv');
const { Client } = require('@elastic/elasticsearch');
const client = new Client({ node: 'http://localhost:9200' });

// Make a request for a user with a given ID
//ExpiredTokenThe provided token has expired.Request signature expired at: 2021-09-13T23:21:03+00:00
axios.get('https://storage.googleapis.com/kagglesdsdata/datasets/552239/1006003/world_country_and_usa_states_latitude_and_longitude_values.csv?X-Goog-Algorithm=GOOG4-RSA-SHA256&X-Goog-Credential=gcp-kaggle-com%40kaggle-161607.iam.gserviceaccount.com%2F20210920%2Fauto%2Fstorage%2Fgoog4_request&X-Goog-Date=20210920T211926Z&X-Goog-Expires=259199&X-Goog-SignedHeaders=host&X-Goog-Signature=0e15f8adf0e642100deb1070d3f66f65a59a8f4493e37c0abfe152d82db33a56171738adce1327e291864b1e412a710a8b3f197e5dff85ed8518aa6a07841cec82e551b62d8c877317399de4cae37d2332501c054c95487200c70b8a872e4678e2ee33e65c7d0ba59ed3ecf1f2bb066d0310474a2aa326b0c9147ee4250d14e6747015e5f122eca74623c4f66fdb91d555d26a50a6aac29506784ac1d9cf24d9f125efbc11d9bda127d9c20a07b90ae7cb18614f6f83559457490e56ad5be7ade4bfa52dd557d276f1831a7f6ca711c100e77998547c26dd3eb2ec0af9f092bc7c4cb34a09efb918aabfc7677f59f60c457c9ec1152a7776d93dd604886cd059')
        .then(function (response) {
        // handle success
//            console.log(response.data);
        asdf = csv.toObjects(response.data);
//            console.log(asdf);
                test = asdf[0]['country_code'];
                console.log(test);
//                asdf.forEach(country => console.log(country));
                asdf.forEach(country =>
                        client.index({
                        index: 'country_code',
                        id: country['country'],
                                // type: '_doc', // uncomment this line if you are using Elasticsearch ≤ 6
                                body: {
                                country_code: country['country_code'],
                                        country: country['country'],
                                        location: {
                                        lat: country['latitude'],
                                                lon: country['longitude']
                                        },
                                        cases: null
                                }
                        })
                        )
        })
        .catch(function (error) {
        // handle error
        console.log(error);
        })
        .then(function () {
        // always executed
        });
