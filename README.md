# Configuration of Elasticsearch on the raspberry pi4 with ubuntu 20 lts server

> Author *`michaelspring123@web.de`*
> > Creation date *`2021-02-21`*
> > > version *`alpha-test`*


# goal of the small project

## raspberry as a server

```mermaid
gantt
section Section
prepare raspberry :done,    des1, 2021-01-06,2021-01-08
install and configure elasticsearch        :active,  des2, 2021-01-07, 3d
install openjdk-14-jre-headless   :         des3, after des1, 1d
install nginx   :         des4, after des1, 1d
install apt-transport-https   :         des5, after des3, 1d
kibana.yml   :         des6, after des4, 1d
```

## use case covid dashboard

```mermaid
graph LR
    id1(collection of data and transformation)-->id2(store in elastic)-->id3(graphic representation kibana)
    style id1 fill:#f9f,stroke:#333,stroke-width:4px
    style id2 fill:#bbf,stroke:#f66,stroke-width:2px,color:#fff,stroke-dasharray: 5 5
    style id3 fill:#f9,stroke:#333,stroke-width:2px
```







# using the raspberry pi manager

The ubuntu image can be created very easily with the raspberry pi manager.

<details>
<summary>Raspberry_Pi_Imager_v1_5</summary>
<p>
![Your_Home_for_Data_Science.jpg](/images/Raspberry_Pi_Imager_v1_5_und_Kaggle__Your_Home_for_Data_Science.jpg)
</p>
</details>

*`Raspberry_Pi_Imager_v1_5`*

<details>
<summary>Raspberry_Pi_Imager_v1_5</summary>
<p>
![Your_Home_for_Data_Science2.jpg](/images/Raspberry_Pi_Imager_v1_5_und_Kaggle__Your_Home_for_Data_Science2.jpg)
</p>
</details> 


## install net-tools

After the raspberry has started, you will be asked to enter the user and the password. The default user is *`ubuntu`* and the password is also *`ubuntu`*.

In order to connect to the server via *`ssh`*, the *`net-tools`* must also be installed.


### update

> *`sudo apt-get update`* 

### net-tools

> *`sudo apt-get install net-tools`*

The ipv4 can be found using *`ifconfig`*.


# Remote access via ssh

> asdf@asdfs-MBP ~ % *`ssh ubuntu@192.168.178.23`*


<details>
<summary>ssh ubuntu@192.168.178.23</summary>
<p>

```console
Last login: Sat Feb 20 22:45:59 on ttys000
asdf@asdfs-MBP ~ % ssh ubuntu@192.168.178.23
The authenticity of host '192.168.178.23 (192.168.178.23)' can't be established.
ECDSA key fingerprint is SHA256:xdCiwMqyhAX2gC0lUFF1s0hcZwGOBgAjbijAlPj1wp0.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '192.168.178.23' (ECDSA) to the list of known hosts.
ubuntu@192.168.178.23's password: 
Welcome to Ubuntu 20.10 (GNU/Linux 5.8.0-1006-raspi aarch64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Sat Feb 20 23:56:44 UTC 2021

  System load:  0.0                Temperature:           53.1 C
  Usage of /:   3.5% of 116.91GB   Processes:             147
  Memory usage: 74%                Users logged in:       1
  Swap usage:   0%                 IPv4 address for eth0: 192.168.178.23

 * Introducing self-healing high availability clusters in MicroK8s.
   Simple, hardened, Kubernetes for production, from RaspberryPi to DC.

     https://microk8s.io/high-availability

102 updates can be installed immediately.
46 of these updates are security updates.
To see these additional updates run: apt list --upgradable


Last login: Sat Feb 20 21:37:47 2021
ubuntu@ubuntu:~$
```
</p>
</details> 



# installation of the java development kit

Basically, there is no java runtime environment in the basic installation. 

## Check java runtime environment

> ubuntu@ubuntu:~$ *`java --version`*

```console
Command 'java' not found, but can be installed with:
sudo apt install default-jre              # version 2:1.11-72, or
sudo apt install openjdk-11-jre-headless  # version 11.0.9+10-0ubuntu1
sudo apt install openjdk-13-jre-headless  # version 13.0.4+8-1
sudo apt install openjdk-14-jre-headless  # version 14.0.2+12-1
sudo apt install openjdk-15-jre-headless  # version 15+36-1
sudo apt install openjdk-8-jre-headless   # version 8u272~b09-0ubuntu1
ubuntu@ubuntu:~$
```


> ubuntu@ubuntu:~$ *`java --version`*

```console
openjdk 14.0.2 2020-07-14
OpenJDK Runtime Environment (build 14.0.2+12-Ubuntu-1)
OpenJDK 64-Bit Server VM (build 14.0.2+12-Ubuntu-1, mixed mode, sharing)
ubuntu@ubuntu:~$
```


# Install Nginx

> ubuntu@ubuntu:~$ *`sudo apt-get install nginx`*


<details>
<summary>Nginx</summary>
<p>

```console
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following additional packages will be installed:
  libgd3 libjbig0 libnginx-mod-http-image-filter
  libnginx-mod-http-xslt-filter libnginx-mod-mail libnginx-mod-stream
  libnginx-mod-stream-geoip2 libtiff5 libwebp6 libxpm4 libxslt1.1
  nginx-common nginx-core
Suggested packages:
  libgd-tools fcgiwrap nginx-doc ssl-cert
The following NEW packages will be installed:
  libgd3 libjbig0 libnginx-mod-http-image-filter
  libnginx-mod-http-xslt-filter libnginx-mod-mail libnginx-mod-stream
  libnginx-mod-stream-geoip2 libtiff5 libwebp6 libxpm4 libxslt1.1 nginx
  nginx-common nginx-core
0 upgraded, 14 newly installed, 0 to remove and 0 not upgraded.
Need to get 1198 kB of archives.
After this operation, 4124 kB of additional disk space will be used.
Do you want to continue? [Y/n] Y
Get:1 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libjbig0 arm64 2.1-3.1build1 [24.0 kB]
Get:2 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libwebp6 arm64 0.6.1-2 [155 kB]
Get:3 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libtiff5 arm64 4.1.0+git191117-2build1 [151 kB]
Get:4 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libxpm4 arm64 1:3.5.12-1 [28.8 kB]
Get:5 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libgd3 arm64 2.3.0-2 [108 kB]
Get:6 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 nginx-common all 1.18.0-6ubuntu2 [39.7 kB]
Get:7 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libnginx-mod-http-image-filter arm64 1.18.0-6ubuntu2 [15.3 kB]
Get:8 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libxslt1.1 arm64 1.1.34-4 [141 kB]
Get:9 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libnginx-mod-http-xslt-filter arm64 1.18.0-6ubuntu2 [14.1 kB]
Get:10 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libnginx-mod-mail arm64 1.18.0-6ubuntu2 [41.6 kB]
Get:11 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libnginx-mod-stream arm64 1.18.0-6ubuntu2 [65.1 kB]
Get:12 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libnginx-mod-stream-geoip2 arm64 1.18.0-6ubuntu2 [9296 B]
Get:13 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 nginx-core arm64 1.18.0-6ubuntu2 [402 kB]
Get:14 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 nginx arm64 1.18.0-6ubuntu2 [3980 B]
Fetched 1198 kB in 0s (3005 kB/s)
Preconfiguring packages ...
Selecting previously unselected package libjbig0:arm64.
(Reading database ... 68714 files and directories currently installed.)
Preparing to unpack .../00-libjbig0_2.1-3.1build1_arm64.deb ...
Unpacking libjbig0:arm64 (2.1-3.1build1) ...
Selecting previously unselected package libwebp6:arm64.
Preparing to unpack .../01-libwebp6_0.6.1-2_arm64.deb ...
Unpacking libwebp6:arm64 (0.6.1-2) ...
Selecting previously unselected package libtiff5:arm64.
Preparing to unpack .../02-libtiff5_4.1.0+git191117-2build1_arm64.deb ...
Unpacking libtiff5:arm64 (4.1.0+git191117-2build1) ...
Selecting previously unselected package libxpm4:arm64.
Preparing to unpack .../03-libxpm4_1%3a3.5.12-1_arm64.deb ...
Unpacking libxpm4:arm64 (1:3.5.12-1) ...
Selecting previously unselected package libgd3:arm64.
Preparing to unpack .../04-libgd3_2.3.0-2_arm64.deb ...
Unpacking libgd3:arm64 (2.3.0-2) ...
Selecting previously unselected package nginx-common.
Preparing to unpack .../05-nginx-common_1.18.0-6ubuntu2_all.deb ...
Unpacking nginx-common (1.18.0-6ubuntu2) ...
Selecting previously unselected package libnginx-mod-http-image-filter.
Preparing to unpack .../06-libnginx-mod-http-image-filter_1.18.0-6ubuntu2_arm64.deb ...
Unpacking libnginx-mod-http-image-filter (1.18.0-6ubuntu2) ...
Selecting previously unselected package libxslt1.1:arm64.
Preparing to unpack .../07-libxslt1.1_1.1.34-4_arm64.deb ...
Unpacking libxslt1.1:arm64 (1.1.34-4) ...
Selecting previously unselected package libnginx-mod-http-xslt-filter.
Preparing to unpack .../08-libnginx-mod-http-xslt-filter_1.18.0-6ubuntu2_arm64.deb ...
Unpacking libnginx-mod-http-xslt-filter (1.18.0-6ubuntu2) ...
Selecting previously unselected package libnginx-mod-mail.
Preparing to unpack .../09-libnginx-mod-mail_1.18.0-6ubuntu2_arm64.deb ...
Unpacking libnginx-mod-mail (1.18.0-6ubuntu2) ...
Selecting previously unselected package libnginx-mod-stream.
Preparing to unpack .../10-libnginx-mod-stream_1.18.0-6ubuntu2_arm64.deb ...
Unpacking libnginx-mod-stream (1.18.0-6ubuntu2) ...
Selecting previously unselected package libnginx-mod-stream-geoip2.
Preparing to unpack .../11-libnginx-mod-stream-geoip2_1.18.0-6ubuntu2_arm64.deb ...
Unpacking libnginx-mod-stream-geoip2 (1.18.0-6ubuntu2) ...
Selecting previously unselected package nginx-core.
Preparing to unpack .../12-nginx-core_1.18.0-6ubuntu2_arm64.deb ...
Unpacking nginx-core (1.18.0-6ubuntu2) ...
Selecting previously unselected package nginx.
Preparing to unpack .../13-nginx_1.18.0-6ubuntu2_arm64.deb ...
Unpacking nginx (1.18.0-6ubuntu2) ...
Setting up libxpm4:arm64 (1:3.5.12-1) ...
Setting up nginx-common (1.18.0-6ubuntu2) ...
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /lib/systemd/system/nginx.service.
Setting up libjbig0:arm64 (2.1-3.1build1) ...
Setting up libwebp6:arm64 (0.6.1-2) ...
Setting up libxslt1.1:arm64 (1.1.34-4) ...
Setting up libtiff5:arm64 (4.1.0+git191117-2build1) ...
Setting up libnginx-mod-mail (1.18.0-6ubuntu2) ...
Setting up libnginx-mod-stream (1.18.0-6ubuntu2) ...
Setting up libnginx-mod-http-xslt-filter (1.18.0-6ubuntu2) ...
Setting up libgd3:arm64 (2.3.0-2) ...
Setting up libnginx-mod-stream-geoip2 (1.18.0-6ubuntu2) ...
Setting up libnginx-mod-http-image-filter (1.18.0-6ubuntu2) ...
Setting up nginx-core (1.18.0-6ubuntu2) ...
 * Upgrading binary nginx                                              [ OK ] 
Setting up nginx (1.18.0-6ubuntu2) ...
Processing triggers for ufw (0.36-7) ...
Processing triggers for systemd (246.6-1ubuntu1) ...
Processing triggers for man-db (2.9.3-2) ...
Processing triggers for libc-bin (2.32-0ubuntu3) ...
ubuntu@ubuntu:~$
```
</p>
</details> 





> ubuntu@ubuntu:~$ *`wget –qO – https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add – `*


<details>
<summary>wget –qO – https://artifacts.elastic.co</summary>
<p>

```console
--2021-02-20 15:26:47--  http://xn--qo-31t/
Resolving xn--qo-31t (xn--qo-31t)... failed: Temporary failure in name resolution.
wget: unable to resolve host address ‘xn--qo-31t’
--2021-02-20 15:26:47--  http://xn--7ug/
Resolving xn--7ug (xn--7ug)... failed: Temporary failure in name resolution.
wget: unable to resolve host address ‘xn--7ug’
--2021-02-20 15:26:47--  https://artifacts.elastic.co/GPG-KEY-elasticsearch
Resolving artifacts.elastic.co (artifacts.elastic.co)... 2600:1901:0:1d7::, 34.120.127.130
Connecting to artifacts.elastic.co (artifacts.elastic.co)|2600:1901:0:1d7::|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 1768 (1.7K) [application/pgp-keys]
Saving to: ‘GPG-KEY-elasticsearch’

GPG-KEY-elasticsear 100%[=================>]   1.73K  --.-KB/s    in 0s      

2021-02-20 15:26:47 (7.25 MB/s) - ‘GPG-KEY-elasticsearch’ saved [1768/1768]

FINISHED --2021-02-20 15:26:47--
Total wall clock time: 0.1s
Downloaded: 1 files, 1.7K in 0s (7.25 MB/s)
Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).
gpg: can't open '–': No such file or directory
```
</p>
</details> 




> ubuntu@ubuntu:~$ *`sudo apt-get install apt-transport-https`*


<details>
<summary>install apt-transport-https</summary>
<p>

```console
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following NEW packages will be installed:
  apt-transport-https
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
Need to get 1704 B of archives.
After this operation, 163 kB of additional disk space will be used.
Get:1 http://ports.ubuntu.com/ubuntu-ports groovy/universe arm64 apt-transport-https all 2.1.10 [1704 B]
Fetched 1704 B in 0s (12.2 kB/s)             
Selecting previously unselected package apt-transport-https.
(Reading database ... 68838 files and directories currently installed.)
Preparing to unpack .../apt-transport-https_2.1.10_all.deb ...
Unpacking apt-transport-https (2.1.10) ...
Setting up apt-transport-https (2.1.10) ...
```
</p>
</details>



> ubuntu@ubuntu:~$ *`echo “deb https://artifacts.elastic.co/packages/7.x/apt stable main” | sudo tee –a /etc/apt/sources.list.d/elastic-7.x.list
“deb https://artifacts.elastic.co/packages/7.x/apt stable main”`*



> ubuntu@ubuntu:~$ *`sudo sh -c 'echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" > /etc/apt/sources.list.d/elastic-7.x.list'`*


> ubuntu@ubuntu:~$ *`sudo apt update`*


<details>
<summary>apt update</summary>
<p>

```console
Get:1 http://ports.ubuntu.com/ubuntu-ports groovy InRelease [267 kB]
Get:2 http://ports.ubuntu.com/ubuntu-ports groovy-updates InRelease [115 kB] 
Get:3 http://ports.ubuntu.com/ubuntu-ports groovy-backports InRelease [101 kB]
Get:4 https://artifacts.elastic.co/packages/7.x/apt stable InRelease [10.4 kB]
Get:5 http://ports.ubuntu.com/ubuntu-ports groovy-security InRelease [110 kB]
Get:6 http://ports.ubuntu.com/ubuntu-ports groovy-updates/main arm64 Packages [297 kB]
Get:7 http://ports.ubuntu.com/ubuntu-ports groovy-updates/main Translation-en [90.3 kB]
Get:8 http://ports.ubuntu.com/ubuntu-ports groovy-updates/main arm64 c-n-f Metadata [6236 B]
Get:9 http://ports.ubuntu.com/ubuntu-ports groovy-updates/restricted arm64 Packages [2812 B]
Get:10 http://ports.ubuntu.com/ubuntu-ports groovy-updates/restricted Translation-en [16.9 kB]
Get:11 http://ports.ubuntu.com/ubuntu-ports groovy-updates/universe arm64 Packages [131 kB]
Get:12 http://ports.ubuntu.com/ubuntu-ports groovy-updates/universe Translation-en [51.2 kB]
Get:13 http://ports.ubuntu.com/ubuntu-ports groovy-updates/universe arm64 c-n-f Metadata [3956 B]
Get:14 http://ports.ubuntu.com/ubuntu-ports groovy-updates/multiverse arm64 Packages [2216 B]
Get:15 http://ports.ubuntu.com/ubuntu-ports groovy-updates/multiverse Translation-en [2124 B]
Get:16 http://ports.ubuntu.com/ubuntu-ports groovy-updates/multiverse arm64 c-n-f Metadata [188 B]
Get:17 http://ports.ubuntu.com/ubuntu-ports groovy-backports/main arm64 c-n-f Metadata [112 B]
Get:18 http://ports.ubuntu.com/ubuntu-ports groovy-backports/restricted arm64 c-n-f Metadata [116 B]
Get:19 http://ports.ubuntu.com/ubuntu-ports groovy-backports/universe arm64 Packages [3928 B]
Get:20 http://ports.ubuntu.com/ubuntu-ports groovy-backports/universe Translation-en [1324 B]
Get:21 http://ports.ubuntu.com/ubuntu-ports groovy-backports/universe arm64 c-n-f Metadata [192 B]
Get:22 http://ports.ubuntu.com/ubuntu-ports groovy-backports/multiverse arm64 c-n-f Metadata [116 B]
Get:23 https://artifacts.elastic.co/packages/7.x/apt stable/main arm64 Packages [25.8 kB]
Get:24 http://ports.ubuntu.com/ubuntu-ports groovy-security/main arm64 Packages [167 kB]
Get:25 http://ports.ubuntu.com/ubuntu-ports groovy-security/main Translation-en [52.2 kB]
Get:26 http://ports.ubuntu.com/ubuntu-ports groovy-security/main arm64 c-n-f Metadata [3252 B]
Get:27 http://ports.ubuntu.com/ubuntu-ports groovy-security/restricted arm64 Packages [2120 B]
Get:28 http://ports.ubuntu.com/ubuntu-ports groovy-security/restricted Translation-en [13.4 kB]
Get:29 http://ports.ubuntu.com/ubuntu-ports groovy-security/universe arm64 Packages [54.5 kB]
Get:30 http://ports.ubuntu.com/ubuntu-ports groovy-security/universe Translation-en [24.7 kB]
Get:31 http://ports.ubuntu.com/ubuntu-ports groovy-security/universe arm64 c-n-f Metadata [2412 B]
Get:32 http://ports.ubuntu.com/ubuntu-ports groovy-security/multiverse Translation-en [352 B]
Fetched 1559 kB in 2s (776 kB/s)        
Reading package lists... Done
Building dependency tree       
Reading state information... Done
98 packages can be upgraded. Run 'apt list --upgradable' to see them.
ubuntu@ubuntu:~$
```
</p>
</details>






> ubuntu@ubuntu:~$ *`sudo apt install elasticsearch`*

<details>
<summary>install elasticsearch</summary>
<p>

```console
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following NEW packages will be installed:
  elasticsearch
0 upgraded, 1 newly installed, 0 to remove and 98 not upgraded.
Need to get 320 MB of archives.
After this operation, 537 MB of additional disk space will be used.
Get:1 https://artifacts.elastic.co/packages/7.x/apt stable/main arm64 elasticsearch arm64 7.11.1 [320 MB]
Fetched 320 MB in 52s (6113 kB/s)                                            
Selecting previously unselected package elasticsearch.
(Reading database ... 68842 files and directories currently installed.)
Preparing to unpack .../elasticsearch_7.11.1_arm64.deb ...
Creating elasticsearch group... OK
Creating elasticsearch user... OK
Unpacking elasticsearch (7.11.1) ...
Setting up elasticsearch (7.11.1) ...
Created elasticsearch keystore in /etc/elasticsearch/elasticsearch.keystore
Processing triggers for systemd (246.6-1ubuntu1) ...
ubuntu@ubuntu:~$
```
</p>
</details>





> ubuntu@ubuntu:~$ *`sudo systemctl enable --now elasticsearch.service`*

```console
Synchronizing state of elasticsearch.service with SysV service script with /lib/systemd/systemd-sysv-install.
Executing: /lib/systemd/systemd-sysv-install enable elasticsearch
Created symlink /etc/systemd/system/multi-user.target.wants/elasticsearch.service → /lib/systemd/system/elasticsearch.service.
ubuntu@ubuntu:~$
```

> ubuntu@ubuntu:~$ *`curl -X GET "localhost:9200/"`*

```json
{
  "name" : "ubuntu",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "i-DtkRJgTZm8tf85LZo_1A",
  "version" : {
    "number" : "7.11.1",
    "build_flavor" : "default",
    "build_type" : "deb",
    "build_hash" : "ff17057114c2199c9c1bbecc727003a907c0db7a",
    "build_date" : "2021-02-15T13:44:09.394032Z",
    "build_snapshot" : false,
    "lucene_version" : "8.7.0",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}
ubuntu@ubuntu:~$
```



> ubuntu@ubuntu:~$ *`sudo nano /etc/elasticsearch/elasticsearch.yml`*


> ubuntu@ubuntu:~$ *`sudo systemctl restart elasticsearch`*

https://stackoverflow.com/questions/58656747/elasticsearch-job-for-elasticsearch-service-failed


> *`sudo nano /etc/elasticsearch/elasticsearch.yml`*

```console
# Pass an initial list of hosts to perform discovery when this node is started:
# The default list of hosts is ["127.0.0.1", "[::1]"]
#
discovery.seed_hosts: []
```



> ubuntu@ubuntu:~$ *`sudo apt install kibana`*


<details>
<summary>install kibana</summary>
<p>

```console
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following NEW packages will be installed:
  kibana
0 upgraded, 1 newly installed, 0 to remove and 98 not upgraded.
Need to get 267 MB of archives.
After this operation, 726 MB of additional disk space will be used.
Get:1 https://artifacts.elastic.co/packages/7.x/apt stable/main arm64 kibana arm64 7.11.1 [267 MB]
Fetched 267 MB in 44s (6012 kB/s)                                            
Selecting previously unselected package kibana.
(Reading database ... 69923 files and directories currently installed.)
Preparing to unpack .../kibana_7.11.1_arm64.deb ...
Unpacking kibana (7.11.1) ...
Setting up kibana (7.11.1) ...
Creating kibana group... OK
Creating kibana user... OK
Created Kibana keystore in /etc/kibana/kibana.keystore
Processing triggers for systemd (246.6-1ubuntu1) ...
ubuntu@ubuntu:~$
```
</p>
</details>




> ubuntu@ubuntu:~$ *`sudo systemctl enable kibana`*

```console
Synchronizing state of kibana.service with SysV service script with /lib/systemd/systemd-sysv-install.
Executing: /lib/systemd/systemd-sysv-install enable kibana
Created symlink /etc/systemd/system/multi-user.target.wants/kibana.service → /etc/systemd/system/kibana.service.
ubuntu@ubuntu:~$
```


> ubuntu@ubuntu:~$ *`sudo ufw allow 5601`*

```console
Rules updated
Rules updated (v6)
ubuntu@ubuntu:~$
```



> ubuntu@ubuntu:~$ *`echo "kibanaadmin:`openssl passwd -apr1`" | sudo tee -a /etc/nginx/htpasswd.users`*

```console
Password: 
Verifying - Password: 
kibanaadmin:$apr1$U5dBf24x$xoQY7Ahjq.AlUxQQMMUTh.
ubuntu@ubuntu:~$
```




> ubuntu@ubuntu:~$ *`sudo nano /etc/nginx/sites-available/your_domain`*

```console
server {
    listen 80;

    server_name your_domain;

    auth_basic "Restricted Access";
    auth_basic_user_file /etc/nginx/htpasswd.users;

    location / {
        proxy_pass http://localhost:5601;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}
```


> ubuntu@ubuntu:~$ *`sudo ln -s /etc/nginx/sites-available/your_domain /etc/nginx/sites-enabled/your_domain`*


> ubuntu@ubuntu:~$ *`sudo nginx -t`*

```console
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
ubuntu@ubuntu:~$
```

> ubuntu@ubuntu:~$ *`sudo systemctl reload nginx`*



> ubuntu@ubuntu:~$ *`sudo ufw allow 'Nginx Full'`*

```console
Rules updated
Rules updated (v6)
ubuntu@ubuntu:~$
```



# Port configuration Kibana

> ubuntu@ubuntu:~$ *`sudo nano /etc/kibana/kibana.yml`*

```console
# Kibana is served by a back end server. This setting specifies the port to use.
#server.port: 5601
server.port: 5601

# Specifies the address to which the Kibana server will bind. IP addresses and h>
# The default is 'localhost', which usually means remote machines will not be ab>
# To allow connections from remote users, set this parameter to a non-loopback a>
#server.host: "localhost"
server.host: 0.0.0.0
```



> ubuntu@ubuntu:~$ *`sudo systemctl restart kibana`*



https://logz.io/blog/configure-yaml-files-elk-stack/





> ubuntu@ubuntu:~$ *`sudo netstat -tulpn | grep LISTEN`*

```console
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      5931/nginx: master  
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      1647/systemd-resolv 
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      2223/sshd: /usr/sbi 
tcp        0      0 0.0.0.0:5601            0.0.0.0:*               LISTEN      9797/node           
tcp6       0      0 :::9200                 :::*                    LISTEN      9498/java           
tcp6       0      0 :::80                   :::*                    LISTEN      5931/nginx: master  
tcp6       0      0 :::9300                 :::*                    LISTEN      9498/java           
tcp6       0      0 :::22                   :::*                    LISTEN      2223/sshd: /usr/sbi
```



> ubuntu@ubuntu:~$ *`sudo apt update`*

<details>
<summary>apt update</summary>
<p>

```console
Hit:1 http://ports.ubuntu.com/ubuntu-ports groovy InRelease
Get:2 http://ports.ubuntu.com/ubuntu-ports groovy-updates InRelease [115 kB]
Hit:3 https://artifacts.elastic.co/packages/7.x/apt stable InRelease
Get:4 http://ports.ubuntu.com/ubuntu-ports groovy-backports InRelease [101 kB]
Get:5 http://ports.ubuntu.com/ubuntu-ports groovy-security InRelease [110 kB]
Fetched 326 kB in 2s (174 kB/s)  
Reading package lists... Done
Building dependency tree       
Reading state information... Done
98 packages can be upgraded. Run 'apt list --upgradable' to see them.
ubuntu@ubuntu:~$ sudo apt install python3-pip
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following additional packages will be installed:
  binutils binutils-aarch64-linux-gnu binutils-common build-essential cpp
  cpp-10 dpkg-dev fakeroot g++ g++-10 gcc gcc-10 libalgorithm-diff-perl
  libalgorithm-diff-xs-perl libalgorithm-merge-perl libasan6 libatomic1
  libbinutils libc-dev-bin libc6-dev libcc1-0 libcrypt-dev libctf-nobfd0
  libctf0 libdpkg-perl libexpat1-dev libfakeroot libfile-fcntllock-perl
  libgcc-10-dev libgomp1 libisl22 libitm1 liblsan0 libmpc3 libnsl-dev
  libpython3-dev libpython3.8-dev libstdc++-10-dev libtirpc-dev libtsan0
  libubsan1 linux-libc-dev make manpages-dev python-pip-whl python3-dev
  python3-wheel python3.8-dev rpcsvc-proto zlib1g-dev
Suggested packages:
  binutils-doc cpp-doc gcc-10-locales debian-keyring gcc-10-doc
  gcc-multilib autoconf automake libtool flex bison gdb gcc-doc glibc-doc
  bzr libstdc++-10-doc make-doc
The following NEW packages will be installed:
  binutils binutils-aarch64-linux-gnu binutils-common build-essential cpp
  cpp-10 dpkg-dev fakeroot g++ g++-10 gcc gcc-10 libalgorithm-diff-perl
  libalgorithm-diff-xs-perl libalgorithm-merge-perl libasan6 libatomic1
  libbinutils libc-dev-bin libc6-dev libcc1-0 libcrypt-dev libctf-nobfd0
  libctf0 libdpkg-perl libexpat1-dev libfakeroot libfile-fcntllock-perl
  libgcc-10-dev libgomp1 libisl22 libitm1 liblsan0 libmpc3 libnsl-dev
  libpython3-dev libpython3.8-dev libstdc++-10-dev libtirpc-dev libtsan0
  libubsan1 linux-libc-dev make manpages-dev python-pip-whl python3-dev
  python3-pip python3-wheel python3.8-dev rpcsvc-proto zlib1g-dev
0 upgraded, 51 newly installed, 0 to remove and 98 not upgraded.
Need to get 50.1 MB of archives.
After this operation, 208 MB of additional disk space will be used.
Do you want to continue? [Y/n] Y
Get:1 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 binutils-common arm64 2.35.1-1ubuntu1 [212 kB]
Get:2 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libbinutils arm64 2.35.1-1ubuntu1 [484 kB]
Get:3 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libctf-nobfd0 arm64 2.35.1-1ubuntu1 [44.8 kB]
Get:4 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libctf0 arm64 2.35.1-1ubuntu1 [44.0 kB]
Get:5 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 binutils-aarch64-linux-gnu arm64 2.35.1-1ubuntu1 [2026 kB]
Get:6 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 binutils arm64 2.35.1-1ubuntu1 [3356 B]
Get:7 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libc-dev-bin arm64 2.32-0ubuntu3 [28.7 kB]
Get:8 http://ports.ubuntu.com/ubuntu-ports groovy-updates/main arm64 linux-libc-dev arm64 5.8.0-43.49 [1118 kB]
Get:9 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libcrypt-dev arm64 1:4.4.16-1ubuntu1 [111 kB]
Get:10 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 rpcsvc-proto arm64 1.4.2-0ubuntu4 [59.7 kB]
Get:11 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libtirpc-dev arm64 1.2.6-1build1 [185 kB]
Get:12 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libnsl-dev arm64 1.3.0-0ubuntu3 [65.6 kB]
Get:13 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libc6-dev arm64 2.32-0ubuntu3 [1726 kB]
Get:14 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libisl22 arm64 0.22.1-1 [537 kB]
Get:15 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libmpc3 arm64 1.2.0~rc1-1 [42.5 kB]
Get:16 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 cpp-10 arm64 10.2.0-13ubuntu1 [7144 kB]
Get:17 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 cpp arm64 4:10.2.0-1ubuntu1 [27.7 kB]
Get:18 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libcc1-0 arm64 10.2.0-13ubuntu1 [37.2 kB]
Get:19 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libgomp1 arm64 10.2.0-13ubuntu1 [93.7 kB]
Get:20 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libitm1 arm64 10.2.0-13ubuntu1 [24.1 kB]
Get:21 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libatomic1 arm64 10.2.0-13ubuntu1 [9836 B]
Get:22 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libasan6 arm64 10.2.0-13ubuntu1 [317 kB]
Get:23 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 liblsan0 arm64 10.2.0-13ubuntu1 [130 kB]
Get:24 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libtsan0 arm64 10.2.0-13ubuntu1 [302 kB]
Get:25 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libubsan1 arm64 10.2.0-13ubuntu1 [127 kB]
Get:26 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libgcc-10-dev arm64 10.2.0-13ubuntu1 [912 kB]
Get:27 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 gcc-10 arm64 10.2.0-13ubuntu1 [14.6 MB]
Get:28 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 gcc arm64 4:10.2.0-1ubuntu1 [5228 B]
Get:29 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libstdc++-10-dev arm64 10.2.0-13ubuntu1 [1715 kB]
Get:30 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 g++-10 arm64 10.2.0-13ubuntu1 [8095 kB]
Get:31 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 g++ arm64 4:10.2.0-1ubuntu1 [1592 B]
Get:32 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 make arm64 4.3-4ubuntu1 [158 kB]
Get:33 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libdpkg-perl all 1.20.5ubuntu2 [232 kB]
Get:34 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 dpkg-dev all 1.20.5ubuntu2 [758 kB]
Get:35 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 build-essential arm64 12.8ubuntu3 [4636 B]
Get:36 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libfakeroot arm64 1.25.2-1 [26.6 kB]
Get:37 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 fakeroot arm64 1.25.2-1 [62.3 kB]
Get:38 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libalgorithm-diff-perl all 1.19.03-2 [46.6 kB]
Get:39 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libalgorithm-diff-xs-perl arm64 0.04-6 [11.1 kB]
Get:40 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libalgorithm-merge-perl all 0.08-3 [12.0 kB]
Get:41 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libexpat1-dev arm64 2.2.9-1build1 [103 kB]
Get:42 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libfile-fcntllock-perl arm64 0.22-3build4 [33.0 kB]
Get:43 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libpython3.8-dev arm64 3.8.6-1 [3349 kB]
Get:44 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 libpython3-dev arm64 3.8.6-0ubuntu1 [7252 B]
Get:45 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 manpages-dev all 5.08-1 [2290 kB]
Get:46 http://ports.ubuntu.com/ubuntu-ports groovy/universe arm64 python-pip-whl all 20.1.1-2 [1842 kB]
Get:47 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 zlib1g-dev arm64 1:1.2.11.dfsg-2ubuntu4 [154 kB]
Get:48 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 python3.8-dev arm64 3.8.6-1 [515 kB]
Get:49 http://ports.ubuntu.com/ubuntu-ports groovy/main arm64 python3-dev arm64 3.8.6-0ubuntu1 [1212 B]
Get:50 http://ports.ubuntu.com/ubuntu-ports groovy/universe arm64 python3-wheel all 0.34.2-1 [23.8 kB]
Get:51 http://ports.ubuntu.com/ubuntu-ports groovy/universe arm64 python3-pip all 20.1.1-2 [247 kB]
Fetched 50.1 MB in 8s (5937 kB/s)                                          
Extracting templates from packages: 100%
Selecting previously unselected package binutils-common:arm64.
(Reading database ... 125335 files and directories currently installed.)
Preparing to unpack .../00-binutils-common_2.35.1-1ubuntu1_arm64.deb ...
Unpacking binutils-common:arm64 (2.35.1-1ubuntu1) ...
Selecting previously unselected package libbinutils:arm64.
Preparing to unpack .../01-libbinutils_2.35.1-1ubuntu1_arm64.deb ...
Unpacking libbinutils:arm64 (2.35.1-1ubuntu1) ...
Selecting previously unselected package libctf-nobfd0:arm64.
Preparing to unpack .../02-libctf-nobfd0_2.35.1-1ubuntu1_arm64.deb ...
Unpacking libctf-nobfd0:arm64 (2.35.1-1ubuntu1) ...
Selecting previously unselected package libctf0:arm64.
Preparing to unpack .../03-libctf0_2.35.1-1ubuntu1_arm64.deb ...
Unpacking libctf0:arm64 (2.35.1-1ubuntu1) ...
Selecting previously unselected package binutils-aarch64-linux-gnu.
Preparing to unpack .../04-binutils-aarch64-linux-gnu_2.35.1-1ubuntu1_arm64.deb ...
Unpacking binutils-aarch64-linux-gnu (2.35.1-1ubuntu1) ...
Selecting previously unselected package binutils.
Preparing to unpack .../05-binutils_2.35.1-1ubuntu1_arm64.deb ...
Unpacking binutils (2.35.1-1ubuntu1) ...
Selecting previously unselected package libc-dev-bin.
Preparing to unpack .../06-libc-dev-bin_2.32-0ubuntu3_arm64.deb ...
Unpacking libc-dev-bin (2.32-0ubuntu3) ...
Selecting previously unselected package linux-libc-dev:arm64.
Preparing to unpack .../07-linux-libc-dev_5.8.0-43.49_arm64.deb ...
Unpacking linux-libc-dev:arm64 (5.8.0-43.49) ...
Selecting previously unselected package libcrypt-dev:arm64.
Preparing to unpack .../08-libcrypt-dev_1%3a4.4.16-1ubuntu1_arm64.deb ...
Unpacking libcrypt-dev:arm64 (1:4.4.16-1ubuntu1) ...
Selecting previously unselected package rpcsvc-proto.
Preparing to unpack .../09-rpcsvc-proto_1.4.2-0ubuntu4_arm64.deb ...
Unpacking rpcsvc-proto (1.4.2-0ubuntu4) ...
Selecting previously unselected package libtirpc-dev:arm64.
Preparing to unpack .../10-libtirpc-dev_1.2.6-1build1_arm64.deb ...
Unpacking libtirpc-dev:arm64 (1.2.6-1build1) ...
Selecting previously unselected package libnsl-dev:arm64.
Preparing to unpack .../11-libnsl-dev_1.3.0-0ubuntu3_arm64.deb ...
Unpacking libnsl-dev:arm64 (1.3.0-0ubuntu3) ...
Selecting previously unselected package libc6-dev:arm64.
Preparing to unpack .../12-libc6-dev_2.32-0ubuntu3_arm64.deb ...
Unpacking libc6-dev:arm64 (2.32-0ubuntu3) ...
Selecting previously unselected package libisl22:arm64.
Preparing to unpack .../13-libisl22_0.22.1-1_arm64.deb ...
Unpacking libisl22:arm64 (0.22.1-1) ...
Selecting previously unselected package libmpc3:arm64.
Preparing to unpack .../14-libmpc3_1.2.0~rc1-1_arm64.deb ...
Unpacking libmpc3:arm64 (1.2.0~rc1-1) ...
Selecting previously unselected package cpp-10.
Preparing to unpack .../15-cpp-10_10.2.0-13ubuntu1_arm64.deb ...
Unpacking cpp-10 (10.2.0-13ubuntu1) ...
Selecting previously unselected package cpp.
Preparing to unpack .../16-cpp_4%3a10.2.0-1ubuntu1_arm64.deb ...
Unpacking cpp (4:10.2.0-1ubuntu1) ...
Selecting previously unselected package libcc1-0:arm64.
Preparing to unpack .../17-libcc1-0_10.2.0-13ubuntu1_arm64.deb ...
Unpacking libcc1-0:arm64 (10.2.0-13ubuntu1) ...
Selecting previously unselected package libgomp1:arm64.
Preparing to unpack .../18-libgomp1_10.2.0-13ubuntu1_arm64.deb ...
Unpacking libgomp1:arm64 (10.2.0-13ubuntu1) ...
Selecting previously unselected package libitm1:arm64.
Preparing to unpack .../19-libitm1_10.2.0-13ubuntu1_arm64.deb ...
Unpacking libitm1:arm64 (10.2.0-13ubuntu1) ...
Selecting previously unselected package libatomic1:arm64.
Preparing to unpack .../20-libatomic1_10.2.0-13ubuntu1_arm64.deb ...
Unpacking libatomic1:arm64 (10.2.0-13ubuntu1) ...
Selecting previously unselected package libasan6:arm64.
Preparing to unpack .../21-libasan6_10.2.0-13ubuntu1_arm64.deb ...
Unpacking libasan6:arm64 (10.2.0-13ubuntu1) ...
Selecting previously unselected package liblsan0:arm64.
Preparing to unpack .../22-liblsan0_10.2.0-13ubuntu1_arm64.deb ...
Unpacking liblsan0:arm64 (10.2.0-13ubuntu1) ...
Selecting previously unselected package libtsan0:arm64.
Preparing to unpack .../23-libtsan0_10.2.0-13ubuntu1_arm64.deb ...
Unpacking libtsan0:arm64 (10.2.0-13ubuntu1) ...
Selecting previously unselected package libubsan1:arm64.
Preparing to unpack .../24-libubsan1_10.2.0-13ubuntu1_arm64.deb ...
Unpacking libubsan1:arm64 (10.2.0-13ubuntu1) ...
Selecting previously unselected package libgcc-10-dev:arm64.
Preparing to unpack .../25-libgcc-10-dev_10.2.0-13ubuntu1_arm64.deb ...
Unpacking libgcc-10-dev:arm64 (10.2.0-13ubuntu1) ...
Selecting previously unselected package gcc-10.
Preparing to unpack .../26-gcc-10_10.2.0-13ubuntu1_arm64.deb ...
Unpacking gcc-10 (10.2.0-13ubuntu1) ...
Selecting previously unselected package gcc.
Preparing to unpack .../27-gcc_4%3a10.2.0-1ubuntu1_arm64.deb ...
Unpacking gcc (4:10.2.0-1ubuntu1) ...
Selecting previously unselected package libstdc++-10-dev:arm64.
Preparing to unpack .../28-libstdc++-10-dev_10.2.0-13ubuntu1_arm64.deb ...
Unpacking libstdc++-10-dev:arm64 (10.2.0-13ubuntu1) ...
Selecting previously unselected package g++-10.
Preparing to unpack .../29-g++-10_10.2.0-13ubuntu1_arm64.deb ...
Unpacking g++-10 (10.2.0-13ubuntu1) ...
Selecting previously unselected package g++.
Preparing to unpack .../30-g++_4%3a10.2.0-1ubuntu1_arm64.deb ...
Unpacking g++ (4:10.2.0-1ubuntu1) ...
Selecting previously unselected package make.
Preparing to unpack .../31-make_4.3-4ubuntu1_arm64.deb ...
Unpacking make (4.3-4ubuntu1) ...
Selecting previously unselected package libdpkg-perl.
Preparing to unpack .../32-libdpkg-perl_1.20.5ubuntu2_all.deb ...
Unpacking libdpkg-perl (1.20.5ubuntu2) ...
Selecting previously unselected package dpkg-dev.
Preparing to unpack .../33-dpkg-dev_1.20.5ubuntu2_all.deb ...
Unpacking dpkg-dev (1.20.5ubuntu2) ...
Selecting previously unselected package build-essential.
Preparing to unpack .../34-build-essential_12.8ubuntu3_arm64.deb ...
Unpacking build-essential (12.8ubuntu3) ...
Selecting previously unselected package libfakeroot:arm64.
Preparing to unpack .../35-libfakeroot_1.25.2-1_arm64.deb ...
Unpacking libfakeroot:arm64 (1.25.2-1) ...
Selecting previously unselected package fakeroot.
Preparing to unpack .../36-fakeroot_1.25.2-1_arm64.deb ...
Unpacking fakeroot (1.25.2-1) ...
Selecting previously unselected package libalgorithm-diff-perl.
Preparing to unpack .../37-libalgorithm-diff-perl_1.19.03-2_all.deb ...
Unpacking libalgorithm-diff-perl (1.19.03-2) ...
Selecting previously unselected package libalgorithm-diff-xs-perl.
Preparing to unpack .../38-libalgorithm-diff-xs-perl_0.04-6_arm64.deb ...
Unpacking libalgorithm-diff-xs-perl (0.04-6) ...
Selecting previously unselected package libalgorithm-merge-perl.
Preparing to unpack .../39-libalgorithm-merge-perl_0.08-3_all.deb ...
Unpacking libalgorithm-merge-perl (0.08-3) ...
Selecting previously unselected package libexpat1-dev:arm64.
Preparing to unpack .../40-libexpat1-dev_2.2.9-1build1_arm64.deb ...
Unpacking libexpat1-dev:arm64 (2.2.9-1build1) ...
Selecting previously unselected package libfile-fcntllock-perl.
Preparing to unpack .../41-libfile-fcntllock-perl_0.22-3build4_arm64.deb ...
Unpacking libfile-fcntllock-perl (0.22-3build4) ...
Selecting previously unselected package libpython3.8-dev:arm64.
Preparing to unpack .../42-libpython3.8-dev_3.8.6-1_arm64.deb ...
Unpacking libpython3.8-dev:arm64 (3.8.6-1) ...
Selecting previously unselected package libpython3-dev:arm64.
Preparing to unpack .../43-libpython3-dev_3.8.6-0ubuntu1_arm64.deb ...
Unpacking libpython3-dev:arm64 (3.8.6-0ubuntu1) ...
Selecting previously unselected package manpages-dev.
Preparing to unpack .../44-manpages-dev_5.08-1_all.deb ...
Unpacking manpages-dev (5.08-1) ...
Selecting previously unselected package python-pip-whl.
Preparing to unpack .../45-python-pip-whl_20.1.1-2_all.deb ...
Unpacking python-pip-whl (20.1.1-2) ...
Selecting previously unselected package zlib1g-dev:arm64.
Preparing to unpack .../46-zlib1g-dev_1%3a1.2.11.dfsg-2ubuntu4_arm64.deb ...
Unpacking zlib1g-dev:arm64 (1:1.2.11.dfsg-2ubuntu4) ...
Selecting previously unselected package python3.8-dev.
Preparing to unpack .../47-python3.8-dev_3.8.6-1_arm64.deb ...
Unpacking python3.8-dev (3.8.6-1) ...
Selecting previously unselected package python3-dev.
Preparing to unpack .../48-python3-dev_3.8.6-0ubuntu1_arm64.deb ...
Unpacking python3-dev (3.8.6-0ubuntu1) ...
Selecting previously unselected package python3-wheel.
Preparing to unpack .../49-python3-wheel_0.34.2-1_all.deb ...
Unpacking python3-wheel (0.34.2-1) ...
Selecting previously unselected package python3-pip.
Preparing to unpack .../50-python3-pip_20.1.1-2_all.deb ...
Unpacking python3-pip (20.1.1-2) ...
Setting up manpages-dev (5.08-1) ...
Setting up libfile-fcntllock-perl (0.22-3build4) ...
Setting up libalgorithm-diff-perl (1.19.03-2) ...
Setting up binutils-common:arm64 (2.35.1-1ubuntu1) ...
Setting up linux-libc-dev:arm64 (5.8.0-43.49) ...
Setting up libctf-nobfd0:arm64 (2.35.1-1ubuntu1) ...
Setting up libgomp1:arm64 (10.2.0-13ubuntu1) ...
Setting up python3-wheel (0.34.2-1) ...
Setting up libfakeroot:arm64 (1.25.2-1) ...
Setting up libasan6:arm64 (10.2.0-13ubuntu1) ...
Setting up fakeroot (1.25.2-1) ...
update-alternatives: using /usr/bin/fakeroot-sysv to provide /usr/bin/fakeroot (fakeroot) in auto mode
Setting up libtirpc-dev:arm64 (1.2.6-1build1) ...
Setting up rpcsvc-proto (1.4.2-0ubuntu4) ...
Setting up make (4.3-4ubuntu1) ...
Setting up libmpc3:arm64 (1.2.0~rc1-1) ...
Setting up libatomic1:arm64 (10.2.0-13ubuntu1) ...
Setting up libdpkg-perl (1.20.5ubuntu2) ...
Setting up libubsan1:arm64 (10.2.0-13ubuntu1) ...
Setting up libnsl-dev:arm64 (1.3.0-0ubuntu3) ...
Setting up libcrypt-dev:arm64 (1:4.4.16-1ubuntu1) ...
Setting up libisl22:arm64 (0.22.1-1) ...
Setting up python-pip-whl (20.1.1-2) ...
Setting up libbinutils:arm64 (2.35.1-1ubuntu1) ...
Setting up libc-dev-bin (2.32-0ubuntu3) ...
Setting up libalgorithm-diff-xs-perl (0.04-6) ...
Setting up libcc1-0:arm64 (10.2.0-13ubuntu1) ...
Setting up liblsan0:arm64 (10.2.0-13ubuntu1) ...
Setting up cpp-10 (10.2.0-13ubuntu1) ...
Setting up libitm1:arm64 (10.2.0-13ubuntu1) ...
Setting up libalgorithm-merge-perl (0.08-3) ...
Setting up libtsan0:arm64 (10.2.0-13ubuntu1) ...
Setting up libctf0:arm64 (2.35.1-1ubuntu1) ...
Setting up libgcc-10-dev:arm64 (10.2.0-13ubuntu1) ...
Setting up binutils-aarch64-linux-gnu (2.35.1-1ubuntu1) ...
Setting up binutils (2.35.1-1ubuntu1) ...
Setting up dpkg-dev (1.20.5ubuntu2) ...
Setting up python3-pip (20.1.1-2) ...
Setting up gcc-10 (10.2.0-13ubuntu1) ...
Setting up cpp (4:10.2.0-1ubuntu1) ...
Setting up libc6-dev:arm64 (2.32-0ubuntu3) ...
Setting up libstdc++-10-dev:arm64 (10.2.0-13ubuntu1) ...
Setting up g++-10 (10.2.0-13ubuntu1) ...
Setting up gcc (4:10.2.0-1ubuntu1) ...
Setting up libexpat1-dev:arm64 (2.2.9-1build1) ...
Setting up libpython3.8-dev:arm64 (3.8.6-1) ...
Setting up zlib1g-dev:arm64 (1:1.2.11.dfsg-2ubuntu4) ...
Setting up g++ (4:10.2.0-1ubuntu1) ...
update-alternatives: using /usr/bin/g++ to provide /usr/bin/c++ (c++) in auto mode
Setting up build-essential (12.8ubuntu3) ...
Setting up libpython3-dev:arm64 (3.8.6-0ubuntu1) ...
Setting up python3.8-dev (3.8.6-1) ...
Setting up python3-dev (3.8.6-0ubuntu1) ...
Processing triggers for man-db (2.9.3-2) ...
Processing triggers for libc-bin (2.32-0ubuntu3) ...
ubuntu@ubuntu:~$ pip3 --version
pip 20.1.1 from /usr/lib/python3/dist-packages/pip (python 3.8)
ubuntu@ubuntu:~$ pip install urllib3
Requirement already satisfied: urllib3 in /usr/lib/python3/dist-packages (1.25.9)
ubuntu@ubuntu:~$ pip install requests
Requirement already satisfied: requests in /usr/lib/python3/dist-packages (2.23.0)
ubuntu@ubuntu:~$ pip install certifi
Requirement already satisfied: certifi in /usr/lib/python3/dist-packages (2020.4.5.1)
ubuntu@ubuntu:~$ pip install simplejson
Requirement already satisfied: simplejson in /usr/lib/python3/dist-packages (3.17.0)
ubuntu@ubuntu:~$ pip install pandas
Collecting pandas
  Downloading pandas-1.2.2-cp38-cp38-manylinux2014_aarch64.whl (10.0 MB)
     |████████████████████████████████| 10.0 MB 113 kB/s 
Collecting python-dateutil>=2.7.3
  Downloading python_dateutil-2.8.1-py2.py3-none-any.whl (227 kB)
     |████████████████████████████████| 227 kB 6.1 MB/s 
Collecting numpy>=1.16.5
  Downloading numpy-1.20.1-cp38-cp38-manylinux2014_aarch64.whl (12.7 MB)
     |████████████████████████████████| 12.7 MB 6.4 MB/s 
Collecting pytz>=2017.3
  Downloading pytz-2021.1-py2.py3-none-any.whl (510 kB)
     |████████████████████████████████| 510 kB 51 kB/s 
Requirement already satisfied: six>=1.5 in /usr/lib/python3/dist-packages (from python-dateutil>=2.7.3->pandas) (1.15.0)
Installing collected packages: python-dateutil, numpy, pytz, pandas
  WARNING: The scripts f2py, f2py3 and f2py3.8 are installed in '/home/ubuntu/.local/bin' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
Successfully installed numpy-1.20.1 pandas-1.2.2 python-dateutil-2.8.1 pytz-2021.1
ubuntu@ubuntu:~$
```
</p>
</details>






# Installation of Node.js on Linux


> ubuntu@ubuntu:~$ *`sudo apt install nodejs`*

<details>
<summary>install nodejs</summary>
<p>

```console
ubuntu@ubuntu:~$ sudo apt install nodejs
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following additional packages will be installed:
  libc-ares2 libnode72 nodejs-doc
Suggested packages:
  npm
The following NEW packages will be installed:
  libc-ares2 libnode72 nodejs nodejs-doc
0 upgraded, 4 newly installed, 0 to remove and 65 not upgraded.
Need to get 10.3 MB of archives.
After this operation, 44.9 MB of additional disk space will be used.
Do you want to continue? [Y/n] Y
Get:1 http://ports.ubuntu.com/ubuntu-ports groovy-updates/main arm64 libc-ares2 arm64 1.16.1-1ubuntu0.1 [39.0 kB]
Get:2 http://ports.ubuntu.com/ubuntu-ports groovy/universe arm64 libnode72 arm64 12.18.2~dfsg-1ubuntu2 [7837 kB]
Get:3 http://ports.ubuntu.com/ubuntu-ports groovy/universe arm64 nodejs-doc all 12.18.2~dfsg-1ubuntu2 [2424 kB]
Get:4 http://ports.ubuntu.com/ubuntu-ports groovy/universe arm64 nodejs arm64 12.18.2~dfsg-1ubuntu2 [33.3 kB]
Fetched 10.3 MB in 29s (356 kB/s)                                              
Selecting previously unselected package libc-ares2:arm64.
(Reading database ... 198895 files and directories currently installed.)
Preparing to unpack .../libc-ares2_1.16.1-1ubuntu0.1_arm64.deb ...
Unpacking libc-ares2:arm64 (1.16.1-1ubuntu0.1) ...
Selecting previously unselected package libnode72:arm64.
Preparing to unpack .../libnode72_12.18.2~dfsg-1ubuntu2_arm64.deb ...
Unpacking libnode72:arm64 (12.18.2~dfsg-1ubuntu2) ...
Selecting previously unselected package nodejs-doc.
Preparing to unpack .../nodejs-doc_12.18.2~dfsg-1ubuntu2_all.deb ...
Unpacking nodejs-doc (12.18.2~dfsg-1ubuntu2) ...
Selecting previously unselected package nodejs.
Preparing to unpack .../nodejs_12.18.2~dfsg-1ubuntu2_arm64.deb ...
Unpacking nodejs (12.18.2~dfsg-1ubuntu2) ...
Setting up libc-ares2:arm64 (1.16.1-1ubuntu0.1) ...
Setting up libnode72:arm64 (12.18.2~dfsg-1ubuntu2) ...
Setting up nodejs-doc (12.18.2~dfsg-1ubuntu2) ...
Setting up nodejs (12.18.2~dfsg-1ubuntu2) ...
update-alternatives: using /usr/bin/nodejs to provide /usr/bin/js (js) in auto mode
Processing triggers for libc-bin (2.32-0ubuntu3) ...
Processing triggers for man-db (2.9.3-2) ...
ubuntu@ubuntu:~$ 
```
</p>
</details>


> ubuntu@ubuntu:~$ *`node -v`*

v12.18.2


> ubuntu@ubuntu:~$ *`npm -v`*

6.14.8


# daily corona dashboard

![Your_Home_for_Data_Science.jpg](/images/Dash.jpg)


## dashboard overview

![Your_Home_for_Data_Science.jpg](/images/Item-0_und_Item-0_und_Item-0_und_Item-0_und_AppleBluetoothExtra_und_AirPortExtra_und_AppleVolumeExtra_und_BatteryExtra_und_AppleClockExtra_und_Item-0_und_NotificationCenter.jpg)


## corona map

![Your_Home_for_Data_Science.jpg](/images/pomber_github_io_-_Elastic_und_Postman.jpg)
