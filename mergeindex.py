#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
service that brings the maximum number of cases in relation to the geoindex
"""

from elasticsearch import Elasticsearch
from elasticsearch import helpers

es = Elasticsearch("http://localhost:9200")

github_io = es.search(index="pomber.github.io", body={"query": {"match_all": {}}})
print("Got %d Hits:" % github_io['hits']['total']['value'])
for hit in github_io['hits']['hits']:
    print("%(country)s %(date)s: %(confirmed)s" % hit["_source"])

# Return of the maximum confirmed cases of the main index per country
github_io = es.search(index="pomber.github.io", body={
  "aggs": {
    "conf_country": {
      "terms": {
        "field": "country.keyword",
        "size": 200
      },
      "aggs": {
        "max_conf": {
          "max": {
            "field": "confirmed"
          }
        }
      }
    }
  }
})


import json
import pandas as pd

# normalization of the values ​​for the pandas dataframe as a buffer
print("Got %d Hits:" % github_io['hits']['total']['value'])
github_io['aggregations']['conf_country']['buckets']
github_io = pd.json_normalize(github_io['aggregations']['conf_country']['buckets'])

print(github_io.head())

# Query of the country_code index with the location properties of the countries
country_code = es.search(index="country_code", body={"query": {"match_all": {}}})
print("Got %d Hits:" % country_code['hits']['total']['value'])
for hit in country_code['hits']['hits']:
    print("%(country_code)s %(country)s: %(location)s" % hit["_source"])

"""
Got 244 Hits:
CG Congo [Republic]: {'lat': '-0.228021', 'lon': '15.827659'}
CZ Czech Republic: {'lat': '49.817492', 'lon': '15.472962'}
GL Greenland: {'lat': '71.706936', 'lon': '-42.604303'}
GI Gibraltar: {'lat': '36.137741', 'lon': '-5.345374'}
GU Guam: {'lat': '13.444304', 'lon': '144.793731'}
"""

print("Got %d Hits:" % country_code['hits']['total']['value'])
country_code['hits']['hits']
country_code = pd.json_normalize(country_code['hits']['hits'])

print(country_code.head())

#df_cd = pd.merge(country_code['_id'], github_io['key'], how='inner')
# relation of the two data frames to the countries and the maximum confirmed cases
result = pd.merge(country_code, 
                  github_io[['doc_count', 'max_conf.value']],
                  left_on=country_code['_id'],
                  right_on=github_io['key'],
                  how='left')

print(result.head())


from datetime import datetime
from elasticsearch import Elasticsearch
es = Elasticsearch()

result['max_conf.value'] = pd.to_numeric(result['max_conf.value'], errors='coerce')
result = result.dropna(subset=['max_conf.value'])
result['max_conf.value'] = result['max_conf.value'].astype(int)

print(result.head())

# loop the merge frame and write to elastic
for index, row in result.iterrows():
    doc = {
        'country_code': row['_source.country_code'],
        'country': row['_source.country'],
        'location': {
            'lat': row['_source.location.lat'],
            'lon': row['_source.location.lon']
        },
        'cases': int(row['max_conf.value'])
    }
    es.index(index="country_code", id=row['key_0'], body=doc)

print('merge completed')


